#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "geoipservice.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    GeoIPService::GeoIPServiceSoap12* client = new GeoIPService::GeoIPServiceSoap12();

    TNS__GetGeoIPContextResponse Resposta = client->getGeoIPContext();

    ui->lineEdit->setText(Resposta.getGeoIPContextResult().iP());
    ui->lineEdit_2->setText(Resposta.getGeoIPContextResult().countryCode());
    ui->lineEdit_3->setText(Resposta.getGeoIPContextResult().countryName());
}
