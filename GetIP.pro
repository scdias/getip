#-------------------------------------------------
#
# Project created by QtCreator 2017-07-23T20:32:42
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GetIP
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
        geoipservice.cpp
unix{
SOURCES += KDSoapClient/doxygen.cpp \
    KDSoapClient/KDDateTime.cpp \
    KDSoapClient/KDSoapAuthentication.cpp \
    KDSoapClient/KDSoapClientInterface.cpp \
    KDSoapClient/KDSoapClientThread.cpp \
    KDSoapClient/KDSoapEndpointReference.cpp \
    KDSoapClient/KDSoapFaultException.cpp \
    KDSoapClient/KDSoapJob.cpp \
    KDSoapClient/KDSoapMessage.cpp \
    KDSoapClient/KDSoapMessageAddressingProperties.cpp \
    KDSoapClient/KDSoapMessageReader.cpp \
    KDSoapClient/KDSoapMessageWriter.cpp \
    KDSoapClient/KDSoapNamespaceManager.cpp \
    KDSoapClient/KDSoapNamespacePrefixes.cpp \
    KDSoapClient/KDSoapPendingCall.cpp \
    KDSoapClient/KDSoapPendingCallWatcher.cpp \
    KDSoapClient/KDSoapReplySslHandler.cpp \
    KDSoapClient/KDSoapSslHandler.cpp \
    KDSoapClient/KDSoapValue.cpp
}

HEADERS  += mainwindow.h \
            geoipservice.h
unix{
HEADERS += KDSoapClient/KDDateTime.h \
    KDSoapClient/KDSoap.h \
    KDSoapClient/KDSoapAuthentication.h \
    KDSoapClient/KDSoapClientInterface.h \
    KDSoapClient/KDSoapClientInterface_p.h \
    KDSoapClient/KDSoapClientThread_p.h \
    KDSoapClient/KDSoapEndpointReference.h \
    KDSoapClient/KDSoapFaultException.h \
    KDSoapClient/KDSoapGlobal.h \
    KDSoapClient/KDSoapJob.h \
    KDSoapClient/KDSoapMessage.h \
    KDSoapClient/KDSoapMessageAddressingProperties.h \
    KDSoapClient/KDSoapMessageReader_p.h \
    KDSoapClient/KDSoapMessageWriter_p.h \
    KDSoapClient/KDSoapNamespaceManager.h \
    KDSoapClient/KDSoapNamespacePrefixes_p.h \
    KDSoapClient/KDSoapPendingCall.h \
    KDSoapClient/KDSoapPendingCall_p.h \
    KDSoapClient/KDSoapPendingCallWatcher.h \
    KDSoapClient/KDSoapPendingCallWatcher_p.h \
    KDSoapClient/KDSoapReplySslHandler_p.h \
    KDSoapClient/KDSoapSslHandler.h \
    KDSoapClient/KDSoapValue.h
}

FORMS    += mainwindow.ui

QT += network

win32: LIBS += -L$$PWD/lib/ -lkdsoap1

INCLUDEPATH += $$PWD/.
DEPENDPATH += $$PWD/.

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/kdsoap1.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libkdsoap1.a
